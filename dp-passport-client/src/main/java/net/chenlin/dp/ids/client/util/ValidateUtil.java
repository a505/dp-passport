package net.chenlin.dp.ids.client.util;

import net.chenlin.dp.ids.common.util.CommonUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 校验工具类
 * @author zhouchenglin[yczclcn@163.com]
 */
public class ValidateUtil {

    private ValidateUtil() {}

    /**
     * 是否为邮箱
     * @param email
     * @return
     */
    public static boolean isEmail(String email) {
        return match("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}$", email);
    }

    /**
     * 是否为手机号
     * @param mobile
     * @return
     */
    public static boolean isMobile(String mobile) {
        return match("^1[0-9]{10}$", mobile);
    }

    /**
     * 是否为银行卡号(^([1-9]{1})(\d{14}|\d{18})$;^\d{12,20}$\d{6}[- ]\d{10,13}$|^\d{4}[- ]\d{4}[- ]\d{4}[- ]\d{4,7}$)
     * @param bankCardNo
     * @return
     */
    public static boolean isBankCardNo(String bankCardNo) {
        return match("^\\d{12,20}$\\d{6}\\d{10,13}$|^\\d{4}\\d{4}\\d{4}\\d{4,7}$", bankCardNo);
    }

    /**
     * 模式匹配字符串格式
     * @param regex
     * @param str
     * @return
     */
    private static boolean match(String regex, String str) {
        if (CommonUtil.strIsNotEmpty(regex) && CommonUtil.strIsNotEmpty(str)) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(str);
            return matcher.matches();
        }
        return false;
    }

}
