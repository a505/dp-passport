package net.chenlin.dp.ids.common.util;

import net.chenlin.dp.ids.common.constant.TicketConst;

/**
 * ticket生成工具类
 * @author zhouchenglin[yczclcn@163.com]
 */
public class TicketUtil {

    /**
     * 生成authStatus骗票据
     * @return
     */
    public static String getAuthStatusTicket() {
        return TicketConst.AUTH_STATUS_NOT_LOGIN_PREFIX + RandomKeyUtil.nextKey();
    }

}
